
<!-- TOC GitLab -->

- [About](#about)
    - [Bookmarks](#bookmarks)
    - [Passwords](#passwords)
- [Smart Launcher](#smart-launcher)
    - [Setup](#setup)
    - [How-to](#how-to)
    - [Troubleshoot](#troubleshoot)
        - [Can't set smart-launcher as preferred browser](#cant-set-smart-launcher-as-preferred-browser)

<!-- /TOC -->

This project aim is agnostic web browsing. Which will be possible by breaking the monolith and moving core functions to dedicated external tools that can interface agnostically with browsers and wishfully do the job better and more efficiently.

# About
I intend to interface with what already exists and is actively mantained and improve on the idea based on these three core functions: web launcher, bookmarks management and password management. 

## Bookmarks
- [Buku](https://github.com/jarun/buku) is an external CLI bookmarks manager.
- [buku_run](https://github.com/carnager/buku_run) is a rofi wrapper for Buku it has bugs and is not being mantained.
- [buku-rofi](https://gitlab.com/lbnza/buku-rofi) is my fork of buku-run. 
- bukubrown is a [browser extension](https://github.com/samhh/bukubrow-webext) and a native messaging [host](https://github.com/samhh/bukubrow-host) for Buku.  

## Passwords
- [Ripasso](https://github.com/cortex/ripasso) is an external CLI password manager that uses [pass](https://github.com/peff/pass) or [gopass](https://github.com/gopasspw/gopass) as backend. Passwords are stored encrypted with GPG.

# Smart Launcher
I envision an external launcher that automatically opens the right browser/profile combination based on inputted URI and parses keywords to do smart search operations and extra.

One use case that I have in mind is to have a browser/profile with strict privacy settings for general browsing and another one for things where you would like to stay signed on like mail services and social media. In other words an external browser containerization.

## Setup
Install through *Makefile* or *PKGBUILD*.

Edit `.config/smart-browser.conf` and configure your preferred browsers.

*Example:* 

```
browser_default=firefox
browser_signon=chromium
```

Edit `.local/share/signon.txt` and configure your signon patterns.

*Example:*

```
gmail.com
youtube.com
reddit.com
```

## How-to
Set **Smart Launcher** as your default browser or launch manually: `smart-launcher <URI>`

## Troubleshoot
### Can't set smart-launcher as preferred browser
Edit http and https schemas at `$HOME/.config/mimeapps.list`

