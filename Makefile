ifndef PREFIX
  PREFIX=/usr/local
endif

install:
	install -Dm755 smart-launcher $(DESTDIR)$(PREFIX)/bin/smart-launcher
	install -Dm755 smart-launcher.desktop $(DESTDIR)$(PREFIX)/share/applications/smart-launcher.desktop
	
